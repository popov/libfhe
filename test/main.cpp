#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/ui/text/TestRunner.h>

#include <fstream>
#include <fhe.hpp>
#include <chunker.hpp>
#include <chunk_record.hpp>

/////////////////////////////////////////////
class Test_FHE: public CppUnit::TestFixture {

     CPPUNIT_TEST_SUITE (Test_FHE);
     CPPUNIT_TEST (myTest_01);
     CPPUNIT_TEST_SUITE_END ();

public:

     void setUp () {

	  return;
     }
     
     void tearDown () {

	  return;
     }          

	
     void myTest_01 () {

	  fits::HeaderExtractor HE;
	  std::ifstream stream ("test.fit");

	  fits::Header* HD = NULL;
	  HD = HE.GetHeader (stream);


	  if (HD == NULL) {

	       CPPUNIT_ASSERT (false);
	       return;
	  }

	  fits::HeaderIterator it  = HD->Begin ();
	  fits::HeaderIterator end = HD->End ();

	  std::cerr << std::endl;
	  while (it != end) {

	       std::cerr << (*it).GetAsString ();
	       std::cerr << std::endl;
	       ++it;
	  };

	  std::cerr << std::endl;
	  std::cerr << "*" << HD->GetSize () << "*";
	  std::cerr << std::endl;

	  if (HD->GetSize () == 82) {

	       CPPUNIT_ASSERT (true);
	       return;
	  }
	  CPPUNIT_ASSERT (false);
	  return;
     }
};
////////////////////////////////////////////////

/////////////////////////////////////////////
class Test_Record: public CppUnit::TestFixture {

     CPPUNIT_TEST_SUITE (Test_Record);
     CPPUNIT_TEST (myTest_01);
     CPPUNIT_TEST_SUITE_END ();

public:

     void setUp () {

	  return;
     }
     
     void tearDown () {

	  return;
     }          

	
     void myTest_01 () {

	  fits::Record R1 ("KEY", "VAL", "COMMENT");
	  fits::Record R2 ("KEY", "VAL");

	  std::cerr << R1.GetAsString ();
	  std::cerr << std::endl;
	  std::cerr << R2.GetAsString ();
	  std::cerr << std::endl;

	  if (R1.GetAsString () == "key: *KEY*, value: *VAL*, comment: *COMMENT*") {
	       if (R2.GetAsString () == "key: *KEY*, value: *VAL*, comment: **") {
		    CPPUNIT_ASSERT (true);
		    return;
	       }
	  }

	  CPPUNIT_ASSERT (false);
	  return;
     }
};
////////////////////////////////////////////////

/////////////////////////////////////////////
class Test_Chunker: public CppUnit::TestFixture {

     CPPUNIT_TEST_SUITE (Test_Chunker);
     CPPUNIT_TEST (myTest_01);
     CPPUNIT_TEST (myTest_02);
     CPPUNIT_TEST_SUITE_END ();

public:

     void setUp () {

	  return;
     }
     
     void tearDown () {

	  return;
     }          

	
     void myTest_01 () {

	  std::ifstream stream ("test.fit");
	  fits::Chunker CH (stream);


	  int i = 0;
	  while (CH.ReadChunk ()) {

	       i++;
	       std::cerr << i;
	       std::cerr << std::endl;
	  }

	  if (i == 3)  {

	       stream.close ();
	       CPPUNIT_ASSERT (true);
	       return;
	  }

	  stream.close ();
	  CPPUNIT_ASSERT (false);
	  return;
     }

     void myTest_02 () {

	  std::ifstream stream ("test.fit");
	  fits::Chunker CH (stream);

	  int i = 0;
	  while (CH.ReadChunk ()) {

	       fits::ChunkRecord CR;
	       while (CH.ReadChunkRecord (CR)) {

		    i++;
	       }
	  }

	  std::cerr << "\n&" << i << "&\n";

	  if (i == 82) {

	       stream.close ();
	       CPPUNIT_ASSERT (true);
	       return;
	  }

	  stream.close ();
	  CPPUNIT_ASSERT (false);
	  return;
     }
};
////////////////////////////////////////////////

/////////////////////////////////////////////
class Test_ChunkRecord: public CppUnit::TestFixture {

     CPPUNIT_TEST_SUITE (Test_ChunkRecord);
     CPPUNIT_TEST (myTest_01);
     CPPUNIT_TEST (myTest_02);
     CPPUNIT_TEST_SUITE_END ();

public:

     void setUp () {

	  return;
     }
     
     void tearDown () {

	  return;
     }          

	
     void myTest_01 () {

	  fits::ChunkRecord CR ("  KEY  =  VALUE   / COMMENT  ");
	  fits::Record R ( CR.GetRecord () );
	  std::cerr << std::endl;
	  std::cerr << R.GetAsString ();

	  if (R.GetAsString () ==
	      "key: *KEY*, value: *VALUE*, comment: *COMMENT*") {

	       fits::ChunkRecord CR2 ("KEY2=VALUE2/COMMENT2");
	       fits::Record R2 ( CR2.GetRecord () );
	       std::cerr << std::endl;
	       std::cerr << R2.GetAsString ();

	       if (R2.GetAsString () == "key: *KEY2*, value: *VALUE2*, comment: *COMMENT2*") {

		    CPPUNIT_ASSERT (true);
		    return;
	       }
	  }
	  CPPUNIT_ASSERT (false);
	  return;
     }

     void myTest_02 () {

	  std::string S ("  HIERARCH TCS COORD TYPE =  'HA/DEC' / COMMENT  ");
	  fits::ChunkRecord CR (S);
	  fits::Record R ( CR.GetRecord () );
	  std::cerr << std::endl;
	  std::cerr << S;
	  std::cerr << std::endl;
	  std::cerr << R.GetAsString ();
	  std::cerr << std::endl;


	  if (R.GetAsString () ==
	       "key: *HIERARCH TCS COORD TYPE*, value: *HA/DEC*, comment: *COMMENT*") {

		CPPUNIT_ASSERT (true);
		return;
	  }

	  CPPUNIT_ASSERT (false);
	  return;
     }

};
////////////////////////////////////////////////

////////////////////////////////////////////////
CPPUNIT_TEST_SUITE_REGISTRATION (Test_FHE);
CPPUNIT_TEST_SUITE_REGISTRATION (Test_Record);
CPPUNIT_TEST_SUITE_REGISTRATION (Test_Chunker);
CPPUNIT_TEST_SUITE_REGISTRATION (Test_ChunkRecord);


int main () {

     CppUnit::TextUi::TestRunner runner;
     CppUnit::TestFactoryRegistry &registry = CppUnit::TestFactoryRegistry::getRegistry ();
     runner.addTest ( registry.makeTest () );
     runner.run ();
     return 0;
}
/////////////////////////////////////////////
