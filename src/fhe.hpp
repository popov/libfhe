/*
  Copyright (c) 2011, Boris Popov <popov.b@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,

  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <istream>
#include "header.hpp"

#ifndef __fhe_hpp__
#define __fhe_hpp__

namespace fits {

// ------------------------------------------------ //
     class HeaderExtractor {

     public:
	  HeaderExtractor ();
	  ~HeaderExtractor ();

	  Header* GetHeader (std::istream&);

     private:
	  HeaderExtractor (const HeaderExtractor&);
	  HeaderExtractor& operator= (const HeaderExtractor&);

	  Header m_Header;
     };
// ------------------------------------------------ //
}
#endif // __fhe_hpp__
