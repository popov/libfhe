/*
  Copyright (c) 2011, Boris Popov <popov.b@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,

  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <istream>
#include "chunk_record.hpp"

#ifndef __chunker_hpp__
#define __chunker_hpp__

namespace fits {

// ------------------------------------------------ //
     class Chunker {

     public:
	  Chunker (std::istream&);
	  ~Chunker ();

	  bool ReadChunk ();
	  bool ReadChunkRecord (ChunkRecord&);

     private:
	  Chunker (const Chunker&);
	  Chunker& operator= (const Chunker&);

	  std::istream& m_Stream;

	  char      Buffer [2880];
	  const int Size;

	  bool m_IsChunkValid () const;

	  int m_RecNum;
	  const int m_RecNumMax;
	  const int m_RecLength;
     };
// ------------------------------------------------ //
}
#endif // __chunker_hpp__
