/*
  Copyright (c) 2011, Boris Popov <popov.b@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,

  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <string>
#include "record.hpp"

#ifndef __chunk_record_hpp__
#define __chunk_record_hpp__

namespace fits {

// ------------------------------------------------ //
     class ChunkRecord {

     public:
	  ChunkRecord ();
	  ChunkRecord (const char*);
	  ChunkRecord (const String);

	  ~ChunkRecord ();

	  bool IsEnd () const;

	  Record GetRecord () const;

     private:
	  String m_Content;
	  void m_Strip ();

	  void m_GetKeyValue  (String&, String&, String&) const;

	  String m_CleanString (const String) const;
     };
// ------------------------------------------------ //
}
#endif // __chunk_record_hpp__
