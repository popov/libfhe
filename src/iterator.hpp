/*
  Copyright (c) 2011, Boris Popov <popov.b@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,

  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <list>
#include "record.hpp"

#ifndef __iterator_hpp__
#define __iterator_hpp__

namespace fits {

// ------------------------------------------------ //
     class HeaderIterator;
     bool operator == (const HeaderIterator&,
		       const HeaderIterator&);

     bool operator != (const HeaderIterator&,
		       const HeaderIterator&);
// ------------------------------------------------ //

// ------------------------------------------------ //
     class Header;
     class HeaderIterator {

	  friend class Header;
	  friend bool operator == (const HeaderIterator&,
				   const HeaderIterator&);

	  friend bool operator != (const HeaderIterator&,
				   const HeaderIterator&);

     public:
	  ~HeaderIterator ();

	  HeaderIterator& operator++ ();

	  Record& operator * ();

     private:
	  typedef std::list <Record> List;
	  typedef List::iterator Iterator;

	  Iterator m_Iterator;

	  HeaderIterator ();
     };
// ------------------------------------------------ //
}
#endif // __iterator_hpp__
