/*
  Copyright (c) 2011, Boris Popov <popov.b@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,

  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "record.hpp"

// ------------------------------------------------ //
fits::Record::Record (const Key key, const Value val,
		      const Comment comment):
     m_Key     (key),
     m_Value   (val),
     m_Comment (comment) {

     return;
     }

fits::Record::Record (const Key key, const Value value):

     m_Key     (key),
     m_Value   (value) {

     return;
     }

fits::Record::~Record () {

     return;
}

fits::Key fits::Record::GetKey () const {

     return m_Key;
}

fits::Value fits::Record::GetValue () const {

     return m_Value;
}

fits::Comment fits::Record::GetComment () const {

     return m_Comment;
}

fits::String fits::Record::GetAsString () const {

     String result ("");
     result += "key: *";
     result += m_Key;
     result += "*, value: *";
     result += m_Value;
     result += "*, comment: *";
     result += m_Comment;
     result += "*";
     return result;
}
// ------------------------------------------------ //
