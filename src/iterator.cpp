/*
  Copyright (c) 2011, Boris Popov <popov.b@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,

  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "iterator.hpp"

// ------------------------------------------------ //
fits::HeaderIterator::HeaderIterator () {

     return;
}

fits::HeaderIterator::~HeaderIterator () {

     return;
}

fits::HeaderIterator& fits::HeaderIterator::operator++ () {

     m_Iterator++;
     return *this;
}

bool fits::operator == (const HeaderIterator& il,
			const HeaderIterator& ir) {

     if (il.m_Iterator == ir.m_Iterator)  return true;
     return false;
}

bool fits::operator != (const HeaderIterator& il,
			const HeaderIterator& ir) {

     if (il.m_Iterator != ir.m_Iterator)  return true;
     return false;
}

fits::Record& fits::HeaderIterator::operator * () {

     return (*m_Iterator);
}
// ------------------------------------------------ //
