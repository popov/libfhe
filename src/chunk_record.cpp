/*
  Copyright (c) 2011, Boris Popov <popov.b@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,

  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <list>
#include "chunk_record.hpp"

// ------------------------------------------------ //
namespace fits {

     static bool   IsItWhiteSpace (const char);
     static String StringCleaner  (const String);
};
// ------------------------------------------------ //

// ------------------------------------------------ //
static fits::String fits::StringCleaner (const String str) {

     String temp ("");
     if (str.size () == 0)  return temp;
	
     std::list <char> ttemp;
     bool first = true;
     for (size_t i = 0; i < str.size (); i++) {
	
	  char ch = str.at (i);
	  if (first) {

	       if ( IsItWhiteSpace (ch) )  continue;
	       else  first = false;
	  }
	  ttemp.push_front (ch);
     }

     if (ttemp.size () == 0) return "";
	
     std::list <char>::const_iterator it = ttemp.begin ();
     std::list <char>::const_iterator end = ttemp.end ();
     std::list <char> ttemp2;
	
     first = true;
     while (it != end) {

	  char ch = *it;
	  if (first) {

	       if ( IsItWhiteSpace (ch) ) {

		    it++;
		    continue;

	       } else  first = false;
	  }
	  ttemp2.push_front (ch);
	  it++;
     }
	
     if (ttemp2.size () == 0) return "";

     it  = ttemp2.begin ();
     end = ttemp2.end ();

     while (it != end) {

	  temp += (*it);
	  it++;
     }
     return temp;
}

static bool fits::IsItWhiteSpace (const char ch) {

     const bool Success = true;
     const bool Fault = false;

     if (ch == '\r')  return Success;
     if (ch == '\n')  return Success;
     if (ch == ' ' )  return Success;
     if (ch == '\t')  return Success;
     if (ch == '\f')  return Success;
     if (ch == '\v')  return Success;

     return Fault;
}
// ------------------------------------------------ //

// ------------------------------------------------ //
fits::ChunkRecord::ChunkRecord ():

     m_Content ("") {
     return;
}

fits::ChunkRecord::ChunkRecord (const char* str):

     m_Content (str) {
     m_Strip ();
     return;
}

fits::ChunkRecord::ChunkRecord (const String str):

     m_Content (str) {
     m_Strip ();
     return;
}

fits::ChunkRecord::~ChunkRecord () {

     return;
}

void fits::ChunkRecord::m_Strip () {

     if (m_Content == "")  return;
     m_Content = StringCleaner (m_Content);
     return;
}

bool fits::ChunkRecord::IsEnd () const {

     const bool Success = true;
     const bool Fault = false;

     if (m_Content == "")  return Fault;

     std::string::size_type n = m_Content.find ("END");
     if (n == std::string::npos)  return Fault;
     if (n == 0)  return Success;
     return Fault;
}

void fits::ChunkRecord::m_GetKeyValue (String& key,
				       String& value,
				       String& comment) const {
     key = "";
     value = "";
     comment = "";

     if (m_Content == "")  return;
     std::string::size_type n = m_Content.find ("=");
     if (n == std::string::npos) {

	  comment = m_Content;
	  return;
     }
     key = m_CleanString (m_Content.substr (0, n));

     String lasttail (m_CleanString (m_Content.substr
				     ((n + 1),
				      m_Content.size () - (n + 1))));

     if (lasttail.at (0) == '\'') {

	  std::string::size_type nn = lasttail.find ("\'", 1);
	  if (nn == std::string::npos) {

	       std::string::size_type nnn = lasttail.find ("/");

	       if (nnn != std::string::npos) {

		    comment =
			 m_CleanString (lasttail.substr ((nnn + 1),
					  lasttail.size () - nnn));
		    value =
			 m_CleanString ( lasttail.substr (0, nnn) );

	       } else {

		    value = lasttail;
	       }

	  } else {

	       value = m_CleanString (lasttail.substr (1, nn - 1));
	       std::string last (m_CleanString (lasttail.substr
						(nn + 1,
						 lasttail.size () - nn)));

	       std::string::size_type nnn = last.find ("/");

	       if (nnn != std::string::npos) {

		    comment =
			 m_CleanString (last.substr ((nnn + 1),
					  last.size () - nnn));
	       }
	  }

     } else {

	  std::string::size_type nn = lasttail.find ("/");
	  if (nn == std::string::npos) {

	       value = lasttail;
	   
	  } else {

	       value =
		    m_CleanString ( lasttail.substr (0, nn) );
	       comment =
		    m_CleanString (lasttail.substr
					( (nn + 1),
					  lasttail.size () - nn));
	  }
     }
     return;
}

fits::String fits::ChunkRecord::m_CleanString
(const String str) const {

     if (str == "")  return "";
     return ( StringCleaner (str) );
}

fits::Record fits::ChunkRecord::GetRecord () const {

     Key key;
     Value value;
     Comment comment;
     m_GetKeyValue (key, value, comment);
     return Record (key, value, comment);
}
// ------------------------------------------------ //
