/*
  Copyright (c) 2011, Boris Popov <popov.b@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,

  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cstring>
#include "chunker.hpp"

// ------------------------------------------------ //
fits::Chunker::Chunker (std::istream& stream):

     m_Stream    (stream),
     Size        (2880),
     m_RecNum    (0),
     m_RecNumMax (35),
     m_RecLength (80) {

     return;
}

fits::Chunker::~Chunker () {

     return;
}

bool fits::Chunker::ReadChunk () {

     const bool Success = true;
     const bool Fault = false;

     m_RecNum = 0;
     if ( m_Stream.eof () )  return Fault;
     if (! m_Stream.good () )  return Fault;

     m_Stream.read (Buffer, Size);

     if ( m_Stream.eof () )  return Fault;
     if (! m_Stream.good () )  return Fault;

     if (m_Stream.gcount () != Size)  return Fault;

     if (! m_IsChunkValid () )  return Fault;

     return Success;
}

bool fits::Chunker::m_IsChunkValid () const {

     const bool Success = true;
     const bool Fault = false;

     for (int i = 0; i < Size; i++) {

	  if (0x20 <= Buffer[i]) {

	       if (Buffer[i] <= 0x7E)  continue;
	  }
	  return Fault;
     }
     return Success;
}

bool fits::Chunker::ReadChunkRecord (ChunkRecord& cr) {

     const bool Success = true;
     const bool Fault = false;

     if (m_RecNum > m_RecNumMax)  return Fault;

     char temp [m_RecLength + 1];
     temp [m_RecLength] = '\0';
     const void* p = Buffer + m_RecNum*m_RecLength;
     memcpy ((void*) temp, p, m_RecLength);
     ChunkRecord CR (temp);

     if ( CR.IsEnd () )  return Fault;

     cr = CR;
     m_RecNum++;
     return Success;
}
// ------------------------------------------------ //
