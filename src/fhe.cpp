/*
  Copyright (c) 2011, Boris Popov <popov.b@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,

  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "fhe.hpp"
#include "chunker.hpp"

// ------------------------------------------------ //
fits::HeaderExtractor::HeaderExtractor () {

     return;
}

fits::HeaderExtractor::~HeaderExtractor () {

     return;
}

fits::Header* fits::HeaderExtractor::GetHeader
(std::istream& stream) {

     m_Header.m_List.clear ();
     Chunker CH (stream);
     while (CH.ReadChunk ()) {

	  ChunkRecord CR;
	  while (CH.ReadChunkRecord (CR)) {

	       Record R (CR.GetRecord ());
	       m_Header.m_List.push_back (R);
	  }
     }

     if (m_Header.m_List.size () == 0)  return NULL;
     return &m_Header;
}
// ------------------------------------------------ //
