/*
  Copyright (c) 2011, Boris Popov <popov.b@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,

  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <string>

#ifndef __record_hpp__
#define __record_hpp__

namespace fits {

// ------------------------------------------------ //
     typedef std::string String;
     typedef String Key;
     typedef String Value;
     typedef String Comment;
// ------------------------------------------------ //

// ------------------------------------------------ //
     class Record {

     public:
	  Record (const Key, const Value, const Comment);
	  Record (const Key, const Value);

	  ~Record ();

	  Key     GetKey     () const;
	  Value   GetValue   () const;
	  Comment GetComment () const;

	  String GetAsString () const;

     private:
	  Key     m_Key;
	  Value   m_Value;
	  Comment m_Comment;
     };
// ------------------------------------------------ //
}
#endif // __record_hpp__
