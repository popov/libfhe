/*
  Copyright (c) 2011, Boris Popov <popov.b@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,

  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "header.hpp"

// ------------------------------------------------ //
fits::Header::Header () {

     return;
}

fits::Header::~Header () {

     return;
}
 
fits::HeaderIterator fits::Header::Begin () {

     HeaderIterator it;
     it.m_Iterator = m_List.begin ();
     return it;
}

fits::HeaderIterator fits::Header::End () {

     HeaderIterator it;
     it.m_Iterator = m_List.end ();
     return it;
}

size_t fits::Header::GetSize () const {

     return m_List.size ();
}
// ------------------------------------------------ //
