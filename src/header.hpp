/*
  Copyright (c) 2011, Boris Popov <popov.b@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,

  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <list>
#include "record.hpp"
#include "iterator.hpp"

#ifndef __header_hpp__
#define __header_hpp__

namespace fits {

// ------------------------------------------------ //
     class HeaderExtractor;
     class Header {

	  friend class HeaderExtractor;

     public:
	  Header ();
	  ~Header ();

	  HeaderIterator Begin ();
	  HeaderIterator End   ();

	  size_t GetSize () const;

     private:
	  Header (const Header&);
	  Header& operator= (const Header&);
	  
	  typedef std::list <Record> List;
	  List m_List;
     };
// ------------------------------------------------ //
}
#endif // __header_hpp__
